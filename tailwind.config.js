/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./**/*.{html,js}", " './index.html',"],
  theme: {
    colors: {
      primary: "#ff9027",
      secondary: "#6C757D",
      white: "#fff",
      black: "#000",
    },
    extend: {},
  },
  plugins: [],
};
